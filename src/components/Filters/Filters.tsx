import { FC } from 'react'
import { Autocomplete, Box, Button, Checkbox, FormControlLabel, Slider, TextField, Typography } from '@mui/material'
import FilterListRoundedIcon from '@mui/icons-material/FilterListRounded'
import FilterListOffRoundedIcon from '@mui/icons-material/FilterListOffRounded'
import CloseRoundedIcon from '@mui/icons-material/CloseRounded'
import { useRouter } from 'next/router'
import useFilters, { Filter } from '@/components/Filters/useFilters'

interface Props {
    filters: Filter[]
}

const Filters: FC<Props> = ({ filters }) => {
    const router = useRouter()
    const { isOpen, initialValues, values, setFieldValue, handleSubmit, setValues, checkboxFilters, handleClickOpen, remapFilterOptions } =
        useFilters(filters, router)

    return (
        <Box
            sx={(theme) => ({
                display: 'flex',
                flexDirection: 'column',
                gap: 4,
                padding: 5,
                borderRadius: 1,
                maxHeight: '400px',
                overflowY: 'scroll',
                backgroundColor: theme.palette.primary.main
            })}
        >
            <Box sx={{ display: 'flex', flexDirection: 'row-reverse', gap: 2 }}>
                <Button
                    color={'secondary'}
                    startIcon={<CloseRoundedIcon />}
                    variant={'outlined'}
                    onClick={() => {
                        setValues(initialValues)
                        handleSubmit()
                    }}
                >
                    {'Zrušiť Filtre'}
                </Button>
                <Button
                    color={'secondary'}
                    startIcon={isOpen ? <FilterListOffRoundedIcon /> : <FilterListRoundedIcon />}
                    variant={'contained'}
                    onClick={handleClickOpen}
                >
                    {isOpen ? 'Skryť Filtre' : 'Zobraziť Filtre'}
                </Button>
            </Box>
            {isOpen && (
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 1,
                        width: '100%'
                    }}
                >
                    {filters.map((filter) => {
                        const { code, name } = filter
                        switch (filter.type) {
                            case 'multiselect': {
                                const { options } = filter
                                return (
                                    <Autocomplete
                                        key={code}
                                        multiple
                                        size={'small'}
                                        options={remapFilterOptions(options)}
                                        value={
                                            !!values[code]
                                                ? remapFilterOptions(options).filter((option) => values[code].value.includes(option.value))
                                                : []
                                        }
                                        getOptionLabel={(option) => `${option.label} (${option.count})`}
                                        renderInput={(params) => <TextField {...params} placeholder={name} />}
                                        sx={{ width: '100%' }}
                                        onChange={(event, newValues) => {
                                            setFieldValue(
                                                code,
                                                !!newValues.length
                                                    ? {
                                                          type: 'multiselect',
                                                          value: newValues.map(({ value }) => value)
                                                      }
                                                    : null
                                            )
                                            handleSubmit()
                                        }}
                                    />
                                )
                            }
                            case 'range': {
                                const { min, max } = filter
                                return (
                                    <Box key={code} sx={{ marginBottom: 2 }}>
                                        <Typography gutterBottom>{name}</Typography>
                                        <Slider
                                            getAriaLabel={() => name}
                                            value={!!values && !!values[code] ? values[code].value : [min, max]}
                                            onChange={(event: any) =>
                                                setFieldValue(code, {
                                                    type: 'range',
                                                    value: event.target?.value
                                                })
                                            }
                                            onChangeCommitted={() => handleSubmit()}
                                            valueLabelDisplay={'on'}
                                            min={min}
                                            max={max}
                                            color={'secondary'}
                                            size={'small'}
                                            getAriaValueText={(value: number) => `${value}E`}
                                            sx={(theme) => ({
                                                '& .MuiSlider-valueLabel': {
                                                    fontSize: 12,
                                                    fontWeight: 'normal',
                                                    top: 35,
                                                    backgroundColor: 'unset',
                                                    color: theme.palette.text.primary,
                                                    '&:before': {
                                                        display: 'none'
                                                    },
                                                    '& *': {
                                                        background: 'transparent',
                                                        color: theme.palette.mode === 'dark' ? '#fff' : '#000'
                                                    }
                                                }
                                            })}
                                        />
                                    </Box>
                                )
                            }
                        }
                    })}
                    {!!checkboxFilters && !!checkboxFilters.length && (
                        <Box>
                            <Typography gutterBottom>{'Filtrovať podľa'}</Typography>
                            <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', gap: 2 }}>
                                {checkboxFilters.map(({ code, type, name, options = [] }) => (
                                    <FormControlLabel
                                        key={code}
                                        control={
                                            <Checkbox
                                                onChange={() => {
                                                    if (!!values[code]) {
                                                        setFieldValue(code, null)
                                                    } else {
                                                        setFieldValue(code, {
                                                            type: type,
                                                            value: options[0].value
                                                        })
                                                    }
                                                    handleSubmit()
                                                }}
                                                color={'secondary'}
                                                checked={!!values[code]}
                                            />
                                        }
                                        label={name}
                                    />
                                ))}
                            </Box>
                        </Box>
                    )}
                </Box>
            )}
        </Box>
    )
}

export default Filters
