import { FormEvent, SetStateAction, useState } from 'react'
import { FormikErrors, useFormik } from 'formik'
import { NextRouter } from 'next/router'

type Rename<T, K extends keyof T, N extends string> = Pick<T, Exclude<keyof T, K>> & { [P in N]: T[K] }

type FilterType = 'range' | 'multiselect' | 'checkbox'

export type FilterOption = {
    name: string
    slug: string
    value: number
    count: number
}

export type Filter = {
    type: FilterType
    code: string
    global_name: string
    name: string
    options?: FilterOption[]
    position?: string
    display_mode?: string
    min?: number
    max?: number
}

interface UseFiltersResult {
    isOpen: boolean
    initialValues: { [filterCode: string]: { type: FilterOption; value: any } }
    values: { [filterCode: string]: { type: FilterOption; value: any } }
    setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => Promise<FormikErrors<{}>> | Promise<void>
    handleSubmit: (e?: FormEvent<HTMLFormElement> | undefined) => void
    setValues: (values: SetStateAction<{}>, shouldValidate?: boolean | undefined) => Promise<FormikErrors<{}>> | Promise<void>
    checkboxFilters: Filter[]
    handleClickOpen: () => void
    remapFilterOptions: (options: FilterOption[] | undefined) => Rename<FilterOption, 'name', 'label'>[]
}

const useFilters = (filters: Filter[], router: NextRouter): UseFiltersResult => {
    const [isOpen, setIsOpen] = useState<boolean>(false)

    const getInitialFilterValues = (filters) => {
        const initialValues = {}
        filters.forEach(({ code }) => {
            initialValues[code] = null
        })
        return initialValues
    }

    const [initialValues] = useState(getInitialFilterValues(filters))

    const updateQueryParams = async (newValues) => {
        Object.keys(newValues).forEach((slug) => {
            if (!!newValues[slug]) {
                delete router.query[slug]
                switch (newValues[slug].type) {
                    case 'multiselect': {
                        router.query[slug] = newValues[slug].value.reduce((a: string, b: number | string) => `${a},${b}`)
                        break
                    }
                    case 'checkbox': {
                        router.query[slug] = newValues[slug].value
                        break
                    }
                    case 'range': {
                        router.query[slug] = newValues[slug].value.reduce((a: string, b: number | string) => `${a}-${b}`)
                        break
                    }
                }
            } else {
                delete router.query[slug]
            }
        })
        await router.push(router)
    }
    const { values, setFieldValue, handleSubmit, setValues } = useFormik({
        initialValues: initialValues,
        onSubmit: async (newValues) => {
            await updateQueryParams(newValues)
        }
    })
    const checkboxFilters = filters.filter(({ type }) => type === 'checkbox')

    const handleClickOpen = () => {
        setIsOpen(!isOpen)
    }
    const remapFilterOptions = (options: FilterOption[] | undefined): Rename<FilterOption, 'name', 'label'>[] => {
        return !!options
            ? options.map((option) => {
                  const { name, ...rest } = option
                  return { ...rest, label: name }
              })
            : []
    }

    return {
        isOpen,
        initialValues,
        values,
        setFieldValue,
        handleSubmit,
        setValues,
        checkboxFilters,
        handleClickOpen,
        remapFilterOptions
    }
}

export default useFilters
