import { useState } from 'react'
import type { MouseEvent } from 'react'

interface UseHeaderResult {
    anchorElNav: null | HTMLElement
    anchorElUser: null | HTMLElement
    handleOpenNavMenu: (event: MouseEvent<HTMLElement>) => void
    handleCloseNavMenu: (event: MouseEvent<HTMLElement>) => void
    handleOpenUserMenu: (event: MouseEvent<HTMLElement>) => void
    handleCloseUserMenu: (event: MouseEvent<HTMLElement>) => void
}

const useHeader = (): UseHeaderResult => {
    const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null)
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null)

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget)
    }
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget)
    }

    const handleCloseNavMenu = () => {
        setAnchorElNav(null)
    }

    const handleCloseUserMenu = () => {
        setAnchorElUser(null)
    }
    return { anchorElNav, anchorElUser, handleOpenNavMenu, handleCloseNavMenu, handleOpenUserMenu, handleCloseUserMenu }
}

export default useHeader
