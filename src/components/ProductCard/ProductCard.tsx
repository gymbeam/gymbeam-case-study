import React, { FC } from 'react'
import Link from 'next/link'
import { Box, Card, CardActionArea, CardActions, CardContent, CardMedia, IconButton, Rating, Typography } from '@mui/material'
import AddShoppingCartRoundedIcon from '@mui/icons-material/AddShoppingCartRounded'

interface Props {
    id: number
    name: string
    formatted_price: string
    product_url: string
    thumbnail: string
    rating_summary: number
    reviews_count: number
}

const ProductCard: FC<Props> = ({ id, name, formatted_price, product_url, thumbnail, rating_summary, reviews_count }) => {
    return (
        <Card
            key={id}
            sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                height: '100%'
            }}
        >
            <Link href={product_url} target={'_blank'} style={{ textDecoration: 'none', height: '100%' }}>
                <CardActionArea
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        height: '100%',
                        justifyContent: 'space-between'
                    }}
                >
                    <CardMedia component="img" image={thumbnail} alt={name} />
                    <CardContent
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            gap: 2
                        }}
                    >
                        <Typography gutterBottom variant="h5" component="div" color={'text.primary'}>
                            {name}
                        </Typography>
                        <Box
                            sx={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                gap: 1
                            }}
                        >
                            <Rating name="read-only" precision={0.1} value={rating_summary / 20} readOnly />
                            <Typography variant="body2" color="text.secondary">{`${rating_summary}% (${reviews_count})`}</Typography>
                        </Box>
                        <Typography variant="body1" color="text.primary">
                            {formatted_price}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Link>
            <CardActions>
                <IconButton size={'large'}>
                    <AddShoppingCartRoundedIcon fontSize={'large'} />
                </IconButton>
            </CardActions>
        </Card>
    )
}

export default ProductCard
