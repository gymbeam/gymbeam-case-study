import { FC } from 'react'
import { Box, Typography } from '@mui/material'

const ErrorComponent: FC = () => {
    return (
        <Box>
            <Typography variant={'h1'}>{'404'}</Typography>
            <Typography variant={'h2'}>{'Neočakávaná chyba pri získavaní dát'}</Typography>
        </Box>
    )
}

export default ErrorComponent
