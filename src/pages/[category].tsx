import React, { FC } from 'react'
import { InferGetServerSidePropsType } from 'next'
import { Box, Unstable_Grid2 as Grid, Typography } from '@mui/material'
import Filters from '@/components/Filters'
import ProductCard from '@/components/ProductCard'
import { CATEGORY_URLS, getFetchUrl } from '@/utils'

type GymBeamCategory = {
    items: any[]
    filters: any[]
    meta: any
    name: string
}

export const getServerSideProps = async (context) => {
    const {
        query: { category: categoryUrl, ...queryParams }
    } = context
    const categoryFetchUrl = getFetchUrl(queryParams, categoryUrl)
    try {
        const categoryPromise = await fetch(categoryFetchUrl, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const category: GymBeamCategory = { ...(await categoryPromise.json()), name: CATEGORY_URLS[categoryUrl].name }
        return {
            props: { category: category }
        }
    } catch (error) {
        return {
            notFound: true
        }
    }
}

const Category: FC<InferGetServerSidePropsType<typeof getServerSideProps>> = ({ category }) => {
    const { items = [], filters = [] } = category
    return (
        <Box sx={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
            <Typography variant={'h1'}>{category.name}</Typography>
            <Filters filters={filters} />
            {!!items.length ? (
                <Grid container spacing={2}>
                    {items.map(({ id, name, thumbnail, formatted_price, product_url, rating_summary, reviews_count }) => (
                        <Grid key={id} xs={12} sm={4} md={3}>
                            <ProductCard
                                id={id}
                                name={name}
                                thumbnail={thumbnail}
                                formatted_price={formatted_price}
                                product_url={product_url}
                                rating_summary={rating_summary}
                                reviews_count={reviews_count}
                            />
                        </Grid>
                    ))}
                </Grid>
            ) : (
                <Typography variant={'h3'}>{'No Products Found'}</Typography>
            )}
        </Box>
    )
}

export default Category
