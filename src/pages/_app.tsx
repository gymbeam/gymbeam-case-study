import type { AppProps } from 'next/app'
import { FC } from 'react'
import { Container, ThemeProvider } from '@mui/material'
import Header from '@/components/Header'
import theme from '@/styles/theme'

const App: FC<AppProps> = ({ Component, pageProps }) => {
    return (
        <ThemeProvider theme={theme}>
            <Header />
            <Container sx={{ paddingBlock: '40px' }}>
                <Component {...pageProps} />
            </Container>
        </ThemeProvider>
    )
}

export default App
