import { createTheme } from '@mui/material'

const theme = createTheme({
    palette: {
        primary: { main: '#ececec' },
        secondary: { main: '#ff4100' },
        text: { primary: '#000000', secondary: '#FFFFFF' }
    }
})

theme.typography.button = {
    [theme.breakpoints.down('xs')]: {
        fontSize: '12px'
    }
}
theme.typography.body1 = {
    [theme.breakpoints.down('xs')]: {
        fontSize: '12px'
    }
}
theme.typography.body2 = {
    [theme.breakpoints.down('xs')]: {
        fontSize: '12px'
    }
}
theme.typography.h1 = {
    [theme.breakpoints.down('xs')]: {
        fontSize: '24px'
    }
}
theme.typography.h5 = {
    [theme.breakpoints.down('xs')]: {
        fontSize: '16px'
    }
}

export default theme
