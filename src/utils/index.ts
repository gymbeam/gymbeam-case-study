export const CATEGORY_URLS: { [url: string]: { id: number; name: string } } = {
    'sports-nutrition': {
        id: 2416,
        name: 'Športová výživa'
    }
}

export const getFetchUrl = (queryParams: any, categoryUrl: string): URL => {
    const categoryFetchUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}catalog/products`)
    categoryFetchUrl.searchParams.set('category_ids', CATEGORY_URLS[categoryUrl].id.toString())
    Object.keys(queryParams).forEach((queryParamKey) =>
        categoryFetchUrl.searchParams.set(
            `${queryParamKey}${queryParams[queryParamKey].toString().includes('-') ? '' : '[]'}`,
            queryParams[queryParamKey].toString()
        )
    )
    return categoryFetchUrl
}
